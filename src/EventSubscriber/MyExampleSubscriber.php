<?php

namespace Drupal\example_mod\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class MyExampleSubscriber implements EventSubscriberInterface {

  /**
   * @param GetResponseEvent $event
   */
  public function checkForRedirection(GetResponseEvent $event) {
    if ($event->getRequest()->getPathInfo() == '/cust-page') {
      $url = \Drupal::url('example_mod.mypage2');
    //  $event->setResponse(new RedirectResponse($url));
    }
  }
  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('checkForRedirection');
    return $events;
  }
}