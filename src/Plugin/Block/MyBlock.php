<?php
/**
 * @file
 * Contains \Drupal\internetdevels\Plugin\Block\AboutLvivBlock.
 */


namespace Drupal\example_mod\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * @Block(
 *   id = "myblock",
 *   admin_label = @Translation("MyBlock")
 * )
 */
class MyBlock extends BlockBase {

  public function build() {
    return array(
      '#markup' => '<div class="myblock1">
<b><p>Creating a custom block defined by your module involves the following steps:</p></b>
<li>Create a block plugin using annotations</li>
<li>Extend the Drupal\Core\Block\BlockBase class.</li>
<li>Implement the necessary methods from the Drupal\Core\Block\BlockPluginInterface interface depending on your use case</li>
        </div>',
    );
  }
}