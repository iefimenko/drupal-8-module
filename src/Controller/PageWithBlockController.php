<?php

namespace Drupal\example_mod\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\example_mod\Plugin\Block\MyBlock;


class PageWithBlockController extends ControllerBase {

  public function pagecontent() {
    $block = MyBlock::build();
    $build = array(
      '#type' => 'markup',
      '#markup' => $block['#markup'],
    );
    return $build;
  }
}
