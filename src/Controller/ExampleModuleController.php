<?php

namespace Drupal\example_mod\Controller;

use Drupal\Core\Controller\ControllerBase;

class ExampleModuleController extends ControllerBase {

  public function pagecontent() {
    $build = array(
      '#type' => 'markup',
      '#markup' => t('Here is the text.'),
    );
    return $build;
  }
}
